/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

/**
 * A KMer Tokeniser for Lucene.
 *
 * @author Darran Kartaschew
 */
public class KMerTokenizer extends Tokenizer {

    /**
     * The sequence which extracts each kmer from the raw sequence.
     */
    private Sequence sequence;
    /*
     * Lucene attribure handlers.
     */
    private CharTermAttribute termAtt;
    private OffsetAttribute offsetAtt;
    private TypeAttribute typeAtt;

    /**
     * Create a new tokeniser for the given input. The sequencer will use default kmer length between 5 and 20 for
     * tokenisation.
     *
     * @param input The input reader stream.
     * @throws SequenceException If the input reader is invalid.
     */
    public KMerTokenizer(Reader input) throws SequenceException {
        super(input);
        sequence = new Sequence(input);
        termAtt = addAttribute(CharTermAttribute.class);
        offsetAtt = addAttribute(OffsetAttribute.class);
        typeAtt = addAttribute(TypeAttribute.class);
    }

    /**
     * Create a new tokeniser for the given input, using kmer lengths between min and max.
     *
     * @param input The input reader stream.
     * @param minimumKMerLength The minimum length kmers to extract from the input.
     * @param maximumKMerLength The maximum length kmers to extract from the input.
     * @throws SequenceException If the input reader is invalid, or lengths specified are invalid.
     */
    public KMerTokenizer(Reader input, int minimumKMerLength, int maximumKMerLength) throws SequenceException {
        super(input);
        sequence = new Sequence(input, minimumKMerLength, maximumKMerLength);
        termAtt = addAttribute(CharTermAttribute.class);
        offsetAtt = addAttribute(OffsetAttribute.class);
        typeAtt = addAttribute(TypeAttribute.class);
    }

    @Override
    public void reset() throws IOException {
        super.reset();
    	sequence.reset(input);
    }

    @Override
    public final boolean incrementToken() throws IOException {
        clearAttributes();
        KMer word;
        try {
            word = sequence.next();
        } catch (SequenceException ex) {
            Logger.getLogger(KMerTokenizer.class.getName()).log(Level.SEVERE, null, ex);
            end();
            return false;
        }
        if (word != null) {
//            System.out.append(String.format("Adding word: %12d - %s - %d - %d\n", 
//                    word.getOffset(), word.getString(), word.getLength(), word.getEndOffset() ));
            termAtt.copyBuffer(word.getKMer(), 0, word.getLength());
            offsetAtt.setOffset(word.getOffset(), word.getEndOffset());
            typeAtt.setType(word.getType());
            return true;
        } else {
            end();
            return false;
        }
    }
}
