/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A sequence of DNA, that which to extract kmers from.
 *
 * @author Darran Kartaschew
 */
public final class Sequence {

    /**
     * The reader buffer size to use.
     */
    private final int BUFFER_SIZE = 16*1024; // 16KiB
    /**
     * The maximum length of the kmer to extract from the input sequence.
     */
    private final int MAX_KMER_LENGTH;
    /**
     * The minimum length of the kmer to extract from the input sequence.
     */
    private final int MIN_KMER_LENGTH;
    /**
     * The reader index for attaching to the kmer for Lucene indexing.
     */
    private int readedIdx = -1;
    /**
     * The input reader that support buffered input to allow us to modify (slightly) the current starting offset to be
     * read.
     */
    private PeekableBufferedReader reader;
    /**
     * A queue of kmers already extracted from the sequence.
     */
    private Queue<KMer> bufWord;

    /**
     * Create a new sequence, with the given input, and extract kmers of the specified lengths.
     *
     * @param input The input source.
     * @param minimumKmerLength The minimum size kmer to extract
     * @param maxKmerLength The maximum size kmer to extract
     * @throws SequenceException If the kmer lengths are invalid (negative, zero or min is greater than max)<br>If the
     * input is invalid or not ready to be read.
     */
    public Sequence(Reader input, int minimumKmerLength, int maxKmerLength) throws SequenceException {
        if (minimumKmerLength <= 0 || maxKmerLength <= 0 || minimumKmerLength > maxKmerLength) {
            throw new SequenceException("The minimum length or maximum length values are invalid, either being less than 1, or min is greater than max");
        }
        MIN_KMER_LENGTH = minimumKmerLength;
        MAX_KMER_LENGTH = maxKmerLength;
        reset(input);
    }

    /**
     * Create a new sequence, with the given input, extracting kmers between k = 5 and k = 20.
     *
     * @param input The input source.
     * @throws SequenceException If the input is invalid or not ready to be read.
     */
    public Sequence(Reader input) throws SequenceException {
        this(input, 5, 20);
    }

    /**
     * Reset the input sequence and provide a new input source.
     *
     * @param input The input source.
     */
    public void reset(Reader input) {
        try {
            this.reader = new PeekableBufferedReader(input, BUFFER_SIZE);
        } catch (IOException ex) {
            Logger.getLogger(Sequence.class.getName()).log(Level.SEVERE, null, ex);
            this.reader = null;
        }
        bufWord = new LinkedList<>();
        readedIdx = -1;
    }

    /**
     * Read the next character buffer to extract kmers from.
     *
     * @return A buffer of characters upto the length of MAX_KMER_LENGTH, or null if nothing to be read.
     */
    private char[] readNext() {
        readedIdx++;
        char[] buf = new char[MAX_KMER_LENGTH];
        int read;
        try {
            read = reader.read(buf, MAX_KMER_LENGTH);
            reader.seek(1);
        } catch (IOException | NullPointerException ex) {
            return null;
        }
        if (read != MAX_KMER_LENGTH) {
            if (read != -1) {
                // resize the buffer due to not reading all requested 20 chars.
                char[] newbuf = new char[read];
                System.arraycopy(buf, 0, newbuf, 0, read);
                buf = newbuf;
            } else {
                return null;
            }
        }
        return buf;
    }

    /**
     * Get the next kmer from the sequence.
     *
     * @return The next kmer or null if no more kmers exist in the sequence.
     * @throws SequenceException If an exception occurred creating a new kmer from the sequence.
     */
    public KMer next() throws SequenceException {
        KMer word = bufWord.poll();
        if (word == null) {
            char[] nextSetOfKmers = readNext();
            if (nextSetOfKmers == null) {
                return null;
            }
            int maxLength = Math.min(MAX_KMER_LENGTH, nextSetOfKmers.length);
            for (int i = MIN_KMER_LENGTH; i <= maxLength; i++) {
                // Create kmers of the given length
                KMer kmer = KMer.create(nextSetOfKmers, 0, i, readedIdx, KMer.TYPE_WORD);
                bufWord.add(kmer);
            }

            word = bufWord.poll();
        }
        return word;
    }
}
